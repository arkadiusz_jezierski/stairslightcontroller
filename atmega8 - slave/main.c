
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
//#include <string.h>

#include <avr/eeprom.h>
#include <util/delay.h>
#include "board.h"
#include "tlc5947.h"
#include "spi.h"
#include "i2c.h"


volatile unsigned char i2c_command = 0;

SIGNAL(TWI_vect) {
    switch (TWSR & 0xf8) {
        case 0x60:
            i2c_command = 0;
            break;

        case 0x80:
            i2c_command = TWDR;
            break;

        case 0xA0:
            // if (i2c_received_byte[0] == I2C_COMMAND)
            //     setFunction(i2c_received_byte[1]);

            break;
    }

    TWCR = (1 << TWINT) | (1 << TWEA) | (1 << TWIE) | (1 << TWEN);

}

SIGNAL (TIMER0_OVF_vect) {

}






///////////////************************** MAIN FUNCTION  **************************////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
///##############################################################################################///

void initTimer(void);

void initTimer() {
    TCCR0 |= (1 << CS00); // clk/8
    TCNT0 = 10;
    TIMSK |= (1 << TOIE0);
}


int main(void) {

	tlcInit();
	i2cInit();

	sei();


	while (1) 
	{

		

	//	if (i2c_command) {

		//	sbi(PORTC,4);

		 for (unsigned int duty = 0; duty < 240; duty++) {
		 	for (int i = 0; i < 16; i++) {
		 		pwm[i] = (unsigned char)duty;
		 	}
		 	writePWM();
			
			
		 	_delay_ms(10);
		}

		 for (unsigned char duty = 239; duty > 0; duty--) {
		 	for (int i = 0; i < 16; i++) {
		 		pwm[i] = duty;
		 	}
		 	writePWM();
			
			
			_delay_ms(10);
		 }
			
			
			
	//	} else {
			//cbi(PORTC,4);
			
			
			// resetPWM();
			// writePWM();
			// _delay_ms(1000);
	//	}
		
		//_delay_ms(1000);


		

		//_delay_ms(1000);
	}



} //END