#ifndef BOARD_H
#define BOARD_H

#define sbi(PORT,BIT) PORT|=_BV(BIT)
#define cbi(PORT,BIT) PORT&=~_BV(BIT)
#define tbi(PORT,BIT) PORT^=_BV(BIT)

#define BLANK_PORT	PORTC
#define BLANK_DIR	DDRC
#define BLANK_PIN	1

#define XLAT_PORT	PORTC
#define XLAT_DIR	DDRC
#define XLAT_PIN	2

#define SPI_CLK_PORT	PORTB
#define SPI_CLK_DIR		DDRB
#define SPI_CLK_PIN		5

#define SPI_DATA_PORT	PORTB
#define SPI_DATA_DIR	DDRB
#define SPI_DATA_PIN	3

#define SPI_SS_PORT		PORTB
#define SPI_SS_DIR		DDRB
#define SPI_SS_PIN		2

#define I2C_CLK_PORT	PORTC
#define I2C_CLK_DIR		DDRC
#define I2C_CLK_PIN		5

#define I2C_DATA_PORT	PORTC
#define I2C_DATA_DIR	DDRC
#define I2C_DATA_PIN	4

#endif