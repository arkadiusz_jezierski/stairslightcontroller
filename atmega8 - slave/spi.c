#include "spi.h"
#include <avr/io.h>
#include "board.h"

void spiInit() {

	sbi(SPI_DATA_DIR,SPI_DATA_PIN);
	sbi(SPI_CLK_DIR,SPI_CLK_PIN);
	sbi(SPI_SS_DIR,SPI_SS_PIN);//SS as output

	CLR_CLK;
	CLR_DATA;

	SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR1);

}

void spiMasterTransmit(char cData) {
	/* Start transmission */
	SPDR = cData;
	/* Wait for transmission complete */
	while(!(SPSR & (1<<SPIF)));
}
