#ifndef SPI_H
#define SPI_H

#define	SET_CLK sbi(SPI_CLK_PORT,SPI_CLK_PIN)
#define	CLR_CLK cbi(SPI_CLK_PORT,SPI_CLK_PIN)

#define	SET_DATA  sbi(SPI_DATA_PORT,SPI_DATA_PIN)
#define	CLR_DATA  cbi(SPI_DATA_PORT,SPI_DATA_PIN)


void spiInit(void);
void spiMasterTransmit(char cData);

#endif