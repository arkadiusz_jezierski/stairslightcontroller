#include "i2c.h"
#include <avr/io.h>
#include "board.h"

void i2cInit(void) {

	sbi(I2C_CLK_DIR,I2C_CLK_PIN);
	sbi(I2C_DATA_DIR,I2C_DATA_PIN);
	
	TWBR=8;
	TWAR=0xAA;
	TWCR=0b01000101; //(1<<TWEA)|(1<<TWEN)|(1<<TWIE)
}