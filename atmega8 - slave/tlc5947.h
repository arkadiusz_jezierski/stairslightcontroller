#ifndef TLC5947_H
#define TLC5947_H

#define SET_BLANK sbi(BLANK_PORT,BLANK_PIN)
#define CLR_BLANK cbi(BLANK_PORT,BLANK_PIN)

#define SET_XLAT  sbi(XLAT_PORT,XLAT_PIN)
#define CLR_XLAT  cbi(XLAT_PORT,XLAT_PIN)

#define	LED_QNTY	16
#define       PWM_MAX_VAL   240
#define OFFSET (288 - (LED_QNTY * 12)) / 8;

unsigned char pwm[LED_QNTY];

extern unsigned int pwmMap[PWM_MAX_VAL];


void tlcInit(void);

void writePWMToBuff(unsigned char *buffer);

void putBufSPI(unsigned char *wrptr, unsigned char len);

void resetPWM(void);

void writePWM(void);

#endif