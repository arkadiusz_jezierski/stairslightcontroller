#include "tlc5947.h"
#include <avr/io.h>
#include "board.h"
#include "spi.h"
#include <string.h>

unsigned int pwmMap[PWM_MAX_VAL] = 
{  0,   0,   0,   1,   2,   4,   6,   8,
 11,  14,  17,  21,  25,  29,  34,  39,
 44,  50,  56,  62,  69,  76,  84,  92,
100, 108, 117, 126, 135, 145, 155, 166,
176, 188, 199, 211, 223, 235, 248, 261,
274, 287, 301, 315, 330, 344, 360, 375,
390, 406, 423, 439, 456, 473, 490, 508,
525, 543, 562, 580, 599, 618, 637, 657,
677, 697, 717, 738, 758, 779, 800, 822,
843, 865, 887, 909, 932, 954, 977, 1000,
//
1023, 1046, 1070, 1093, 1117, 1141, 1165, 1190,
1214, 1239, 1263, 1288, 1313, 1338, 1363, 1389,
1414, 1440, 1465, 1491, 1517, 1543, 1569, 1595,
1621, 1647, 1674, 1700, 1726, 1753, 1779, 1806,
1833, 1859, 1886, 1913, 1939, 1966, 1993, 2020,
2047, 2073, 2100, 2127, 2154, 2180, 2207, 2234,
2261, 2287, 2314, 2340, 2367, 2393, 2420, 2446,
2472, 2498, 2525, 2551, 2576, 2602, 2628, 2654,
2679, 2705, 2730, 2755, 2780, 2805, 2830, 2855,
2879, 2904, 2928, 2952, 2976, 3000, 3023, 3047,
//
3070, 3093, 3116, 3139, 3162, 3184, 3206, 3228,
3250, 3271, 3293, 3314, 3335, 3356, 3376, 3396,
3416, 3436, 3456, 3475, 3494, 3513, 3532, 3550,
3568, 3586, 3603, 3621, 3638, 3654, 3671, 3687,
3703, 3718, 3734, 3749, 3763, 3778, 3792, 3806,
3819, 3833, 3846, 3858, 3871, 3883, 3894, 3906,
3917, 3927, 3938, 3948, 3958, 3967, 3976, 3985,
3994, 4002, 4009, 4017, 4024, 4031, 4037, 4043,
4049, 4054, 4059, 4064, 4069, 4073, 4076, 4080,
4082, 4085, 4087, 4089, 4091, 4092, 4093, 4095
};

void tlcInit() {

	spiInit();

	CLR_BLANK;
	CLR_XLAT;

	sbi(BLANK_DIR,BLANK_PIN);
	sbi(XLAT_DIR,XLAT_PIN);
}

void writePWMToBuff(unsigned char *buffer) {
    unsigned int tmpPWM;
    int offset;
    unsigned char i = 0;
    offset = OFFSET
    if ((288 - (LED_QNTY * 12)) % 8) {
        tmpPWM = ~pwmMap[pwm[i++]];
        buffer[offset++] = (tmpPWM >> 8) & 0x0f;
        buffer[offset++] = tmpPWM & 0xff;
    }

    while (i < LED_QNTY) {
        tmpPWM = ~pwmMap[pwm[i++]];
        buffer[offset++] = (unsigned char)((tmpPWM >> 4) & 0xff);
        buffer[offset] = (unsigned char)((tmpPWM << 4) & 0xf0);
        tmpPWM = ~pwmMap[pwm[i++]];
        buffer[offset++] |= (unsigned char)((tmpPWM >> 8) & 0x0f);
        buffer[offset++] = (unsigned char)(tmpPWM & 0xff);
    }
}

void putBufSPI(unsigned char *wrptr, unsigned char len) {
    while (len > 0) {
        spiMasterTransmit(*wrptr++);
        len--;
    }
}

void resetPWM() {
    for (unsigned char i = 0; i < LED_QNTY; i++) {
        pwm[i] = 0;
    }
}

void writePWM() {
    unsigned char total[36];
    memset(total, 0xff, 36);
    writePWMToBuff(total);
    putBufSPI(total, 36);

    SET_XLAT;
	SET_BLANK;
	
	CLR_XLAT;
	CLR_BLANK;

}
