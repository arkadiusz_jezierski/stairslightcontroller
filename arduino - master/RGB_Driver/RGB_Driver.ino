

// Enable debug prints to serial monitor
#define MY_DEBUG 


// Enable and select radio type attached
//#define MY_RADIO_NRF24
//#define MY_RADIO_RFM69

// Set LOW transmit power level as default, if you have an amplified NRF-module and
// power your radio separately with a good regulator you can turn up PA level. 
//#define MY_RF24_PA_LEVEL RF24_PA_LOW

// Enable serial gateway
#define MY_GATEWAY_SERIAL
#define MY_BAUD_RATE 38400
//#define MY_GATEWAY_ESP8266
//#define MY_WIFI_SSID "HomeNetwork_2.4"
//#define MY_WIFI_PASSWORD "@rkadiuszdominikjezierski"
//#define MY_HOSTNAME "ESP8266_RGB"
//#define MY_PORT 5003


#include <MySensors.h>  
#include <Bounce2.h>
#include <Wire.h>

#define RGB_CHILD_ID 13

#define SDA_PIN 4
#define SCL_PIN 5
const int16_t I2C_SLAVE = 0x55;




#define IN_1_PIN  5
#define IN_1_PIN_CHILD_ID  5
#define IN_2_PIN  6
#define IN_2_PIN_CHILD_ID  6

Bounce debouncer1 = Bounce();
Bounce debouncer2 = Bounce();

MyMessage msgInput1(IN_1_PIN_CHILD_ID, V_STATUS);
MyMessage msgInput2(IN_2_PIN_CHILD_ID, V_STATUS);

bool initialInput1ValueSent = false;
bool initialInput2ValueSent = false;


void handleInput1Change(bool newValue) {
  send(msgInput1.set(newValue));
  if (newValue) {
    Serial.println("Sending 1");
    Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
    Wire.write(0x1);   
    Wire.endTransmission();    // stop transmitting
  } else {
    Serial.println("Sending 0");
    Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
    Wire.write(0);   
    Wire.endTransmission();    // stop transmitting
  }
  
}

void handleInput2Change(bool newValue) {
  send(msgInput2.set(newValue));
  Serial.println("Sending 1");
  Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
  Wire.write(0x1);   
  Wire.endTransmission();    // stop transmitting
}





MyMessage msg(RGB_CHILD_ID, V_RGB);
MyMessage msgSwitch( RGB_CHILD_ID, V_STATUS );

bool initialValueSent = false;
bool initialValueSwitchSent = false;

void setup() { 
  Serial.begin(9600);  
  Serial.println("Starting");
  
  Wire.setClock(100000);
  Wire.begin(); // join i2c bus (address optional for master)

  pinMode(IN_1_PIN, INPUT);   //sterowanie stanem wysokim, INPUT_PULLUP - sterowanie stanem niskim
  pinMode(IN_2_PIN, INPUT);   //
  debouncer1.attach(IN_1_PIN);
  debouncer1.interval(10);
  debouncer2.attach(IN_2_PIN);
  debouncer2.interval(10);
}
void presentation()  
{   
  // Send the sketch version information to the gateway and Controller
  sendSketchInfo("RGB Driver", "1.0");

 
  present(RGB_CHILD_ID, S_RGB_LIGHT);

  present(IN_1_PIN_CHILD_ID, S_BINARY);
  present(IN_2_PIN_CHILD_ID, S_BINARY);
//  present(78, S_BINARY);
//  present(79, S_BINARY);
//  present(80, S_BINARY);
//  present(81, S_INFO);
}

int lastInput1Sent = -1;
int lastInput2Sent = -1;


byte x = 0;
void loop() { 
  /*    FOR HA hangling
  if (!initialValueSent) {
    //Serial.println("Sending initial value");
    send(msg.set("000000"));
    //Serial.println("Requesting initial value from controller");
    request(RGB_CHILD_ID, V_RGB);
    //wait(2000, C_SET, V_STATUS);
  }

  if (!initialValueSwitchSent) {
    //Serial.println("Sending initial switch value");
    send(msgSwitch.set(false));
    //Serial.println("Requesting initial value from controller");
    request(RGB_CHILD_ID, V_STATUS);
  }
*/

    if (debouncer1.update()) {
      if (debouncer1.read()==LOW) {
        if (lastInput1Sent != 0) {
          lastInput1Sent = 0;
          handleInput1Change(false);
        }
      
      } else {
        if (lastInput1Sent != 1) {
          lastInput1Sent = 1;
          handleInput1Change(true);
        }
      }
    }
  
    if (debouncer2.update()) {
      if (debouncer2.read()==LOW) {
        if (lastInput2Sent != 0) {
          lastInput2Sent = 0;
          handleInput2Change(false);
        }
      
      } else {
        if (lastInput2Sent != 1) {
          lastInput2Sent = 1;
          handleInput2Change(true);
        }
      }
    }

    if (x == 0) {
         Serial.println("Sending 1");
         Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
         Wire.write(0x1);   
         Wire.endTransmission();    // stop transmitting
    }
    if (x == 10) {
         Serial.println("Sending 0");
         Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
         Wire.write(0);   
         Wire.endTransmission();    // stop transmitting
    }
  

    delay(100);
    x++;

    if (x > 20) x=0;

}


void receive(const MyMessage &message) {

//  Serial.print("message type: ");
//  Serial.println(message.type);
//  Serial.print("message data: ");
//  Serial.println(message.data);
//  Serial.print("message sensor: ");
//  Serial.println(message.sensor);

  if (!initialValueSent && message.type == V_RGB) {
    //Serial.println("Receiving initial value from controller for RGB");
    initialValueSent = true;
  }

  if (!initialValueSwitchSent && message.type == V_STATUS) {
    //Serial.println("Receiving initial value from controller for Switch RGB");
    initialValueSwitchSent = true;
  }
  
  if (message.type == V_RGB) {
     long number = (long) strtol( message.data, NULL, 16);
     Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
     Wire.write(0x14);
     Wire.write(number >> 16);
     Wire.write(number >> 8 & 0xff);
     Wire.write(number & 0xff);          
     Wire.endTransmission();    // stop transmitting
   } 

   if (message.type == V_STATUS) {
    int state = (bool)message.getInt();
    Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
    Wire.write(0x14);
    if (state) {
      Wire.write(0xff);
      Wire.write(0xff);
      Wire.write(0xff);        
    } else {
      Wire.write(0);
      Wire.write(0);
      Wire.write(0);     
    }
    Wire.endTransmission();    // stop transmitting
   } 

//   if (message.type==V_STATUS) {
//
//     int val =  message.sensor;
//
//     if (val == 78) {
//      Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
//      Wire.write(1);          
//      Wire.endTransmission();    // stop transmitting
//     }
//     if (val == 79) {
//      Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
//      Wire.write(2);          
//      Wire.endTransmission();    // stop transmitting
//     }
//     if (val == 80) {
//      Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
//      Wire.write(3);          
//      Wire.endTransmission();    // stop transmitting
//     }
//     
//   } 
//
//   if (message.type==V_TEXT && message.sensor == 81) {
//    Serial.println("type passed");
//    if (strcmp(message.data, "mode1") == 0) {
//      Serial.println("data passed");
//      Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
//      Wire.write(0x01);
//      Wire.endTransmission();    // stop transmitting
//    }
//    if (strcmp(message.data, "mode2") == 0) {
//      Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
//      Wire.write(0x02);
//      Wire.endTransmission();    // stop transmitting
//    }
//    if (strcmp(message.data, "mode3") == 0) {
//      Wire.beginTransmission(I2C_SLAVE); // transmit to device #8
//      Wire.write(0x03);
//      Wire.endTransmission();    // stop transmitting
//    }
//   }
}
